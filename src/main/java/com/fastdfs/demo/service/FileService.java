package com.fastdfs.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;

import java.io.IOException;
import java.io.InputStream;

@Service
public class FileService {

    @Autowired
    private FastFileStorageClient storageClient;

    public String upload(MultipartFile file) throws IOException {
        try (InputStream inputStream = file.getInputStream()) {
            StorePath storePath = storageClient.uploadFile(inputStream, file.getSize(), getExtension(file.getOriginalFilename()),null);
            return storePath.getFullPath();
        }
    }

    private String getExtension(String filename) {
        // 获取文件扩展名
        return filename.substring(filename.lastIndexOf(".") + 1);
    }

}