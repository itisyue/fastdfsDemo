package com.fastdfs.demo.service;

import org.csource.fastdfs.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class FastDFSFileUtil {
    private static Logger log = LoggerFactory.getLogger(FastDFSFileUtil.class);
    private static TrackerServer ts = null;
    private static StorageServer ss = null;
    private static StorageClient sc = null;

    static {
        try {
            //读取FsatDFS的配置文件用于将所有的tracker的地址读取到内存中
            ClientGlobal.init("fastdfs.conf");
            TrackerClient tc = new TrackerClient();
            ts = tc.getTrackerServer();
            ss = tc.getStoreStorage(ts);
            //定义storage的客户端对象，需要使用这个对象来完成具体文件上传 下载和删除
             sc = new StorageClient(ts,ss);
        }catch (Exception e){

        }
    }

    /**
     * 文件上传
     * @param filePath
     * @param fileExtName
     * @return //返回一个String数组 这个数据对我们非常重要，建议存入数据库
     *             //返回值，一个是组名，一个是上传文件存储在的目录
     */
    public static String[] upload(String filePath, String fileExtName) {
        try {
            //文件上传
            //参数一 需要上传的文件的绝对路径
            //参数二 为需要上传的文件的扩展名
            //参数三 为文件的属性文件通常不上传
            //返回一个String数组 这个数据对我们非常重要，建议存入数据库
            //返回值，一个是组名，一个是上传文件存储在的目录
            String[] result = sc.upload_appender_file(filePath,fileExtName,null);
            for (String str:result) {
                log.info(str);
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    //文件下载
    public static Integer download(String groupName, String remoteName, String localFileName) {
        try {
            //文件下载
            //参数一 需要下载的文件的组名
            //参数二 需要下载文件的远程文件名
            //参数三 需要保存到本地的文件名称
            //返回一个int类型的数据，返回0表示文件下载成功其它值表示文件下载失败
            //String groupName = "group1";
            //String remoteName = "M00/00/00/sBRcHmDIlLCEDtsjAAAAAE_bs3A176.png";
            //String localFileName = "d:/download.png";
            int i = sc.download_file(groupName, remoteName,localFileName);
            //System.out.println(i);
            log.info("下载返回结果(0代表成功):"+i);
            return i;
        } catch (Exception  e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static Integer delete(String groupName, String remoteName) {
        try {
            //文件下载
            //参数一 需要删除的文件的组名
            //参数二 需要删除文件的远程文件名
            //返回一个int类型的数据，返回0表示文件删除成功其它值表示文件删除失败
            //String groupName = "group1";
            //String remoteName = "M00/00/00/rBEAB2Yn1MSESf9-AAAAABK7j90639.jpg";
            int i = sc.delete_file(groupName,remoteName);
            log.info("删除返回结果(0代表成功):"+i);
            return i;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }


    public static void main(String[] args) {
//        upload();

        delete("group1", "M00/00/00/rBEAB2Yn1MSESf9-AAAAABK7j90639.jpg");//0表示删除成功
    }


}
