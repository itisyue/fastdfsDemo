package com.fastdfs.demo.util;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

public class OfficeUtil {
    public static void delete(String filePath){
        File file = new File(filePath);
        if(file.exists()){
            file.delete();
        }
    }
    public static File transferToFile(MultipartFile multipartFile, String fileName) {
//        选择用缓冲区来实现这个转换即使用java 创建的临时文件 使用 MultipartFile.transferto()方法 。
        File file = null;
        try {
            //String originalFilename = multipartFile.getOriginalFilename();
            //String[] filename = originalFilename.split("\\.");
            //file = File.createTempFile(filename[0], filename[1]);
            file = new File(fileName);
            multipartFile.transferTo(file);
            //file.deleteOnExit();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }
}
