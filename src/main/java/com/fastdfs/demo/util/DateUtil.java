package com.fastdfs.demo.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

public class DateUtil {
    public static final String DATE_FORMAT_1 = "yyyy-MM-dd";
    public static final String DATE_FORMAT_2 = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_3 = "yyyyMMdd";
    public static final String DATE_FORMAT_4 = "yyyyMMdd HH:mm:ss";
    public static final String DATE_FORMAT_5 = "yyyy-MM";
    public static final String DATE_FORMAT_6 = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String DATE_FORMAT_7 = "yyyyMMddHHmmssSSS";



    public static String getDateStr2(Date date, String format){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String datestr = dateFormat.format(date);
        System.out.println(datestr);
        return datestr;
    }



    public static String getRandomId(){
        Random random = new Random();
        String s = "";
        try{
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
            s = dateFormat.format(new Date());
        }catch (Exception e){
        }
        for(int k=0;k<13;k++){
            s = s + String.valueOf(random.nextInt(10));
        }
        return s.trim();
    }
    public static Date parseDate(String dateTime, String dateFormat){
        if(StringUtils.isBlank(dateTime)){
            return null;
        }
        try{
            return DateUtils.parseDate(dateTime,dateFormat);
        }catch (Exception e){
            return null;
        }
    }
    public static String dateToStr(Date date, String dateFormat){
        if(date==null || StringUtils.isBlank(dateFormat)){
            return null;
        }
        return DateFormatUtils.format(date, dateFormat);
    }
    public static String getDateStr(Date date, String dateFormat){
        if(date==null || StringUtils.isBlank(dateFormat)){
            return "";
        }
        return DateFormatUtils.format(date,dateFormat);
    }
    public static String checkYearMonth(String yearMonth){
        Date date = DateUtil.parseDate(yearMonth, "yyyy.MM");
        if(date==null){
            date = DateUtil.parseDate(yearMonth, "yyyy-MM");
            if(date==null){
                return null;
            }
        }
        String replaceYearMonth = yearMonth.replaceAll("\\.", "-");
        String[] yearMonthArr = replaceYearMonth.split("-");
        int month = Integer.parseInt(yearMonthArr[1]);
        if(month<10){
            return yearMonthArr[0] + "-0"+month;
        }else{
            return  replaceYearMonth;
        }
    }
    public static int getMonthLastDay(Date date){
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public static boolean isLegalDate(int length, String sDate, String format){
        if((sDate==null) || (sDate.length() != length)){
            return false;
        }
        SimpleDateFormat sdf = new SimpleDateFormat();
        try{
            Date date = sdf.parse(sDate);
            return sDate.equals(sdf.format(date));
        }catch (Exception e){
            return false;
        }
    }


}
