package com.fastdfs.demo.dto;

public class ResultUtil {
    public static <T> RestResult<T> success(T data){
        RestResult<T> result = new RestResult<T>(true);
        result.setResult(data);
        return  result;
    }
    public static <T> RestResult<T> success(T data, String msg){
        RestResult<T> result = new RestResult<T>(true);
        result.setResult(data);
        result.setMsg(msg);
        return  result;
    }
    public static <T> RestResult<T> fail(String errorMsg){
        RestResult<T> result = new RestResult<T>(false);
        result.setMsg(errorMsg);
        return  result;
    }
    public static <T> RestResult<T> fail(T data, String errorMsg){
        RestResult<T> result = new RestResult<T>(false);
        result.setMsg(errorMsg);
        result.setResult(data);
        return  result;
    }
}
