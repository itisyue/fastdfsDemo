package com.fastdfs.demo.dto;

import org.slf4j.MDC;

import java.io.Serializable;

public class RestResult<T> implements Serializable {
    private boolean success;
    private T result;
    private String code;
    private String msg;
    private String reqId;

    public RestResult(boolean success){
        this.success  = success;
        if(success){
            this.code = "200";
            this.msg = "成功";
        }else{
            this.code = "0001";
            this.msg = "失败";
        }
    }
    public RestResult(T result){
        this.success  = true;
        this.result = result;
        this.code = "200";
        this.msg = "成功";
    }
    public RestResult(T result, String code){
        this.success  = true;
        this.result = result;
        this.code = code;
    }
    public RestResult(boolean success, String code, String msg){
        this.success  = success;
        this.msg = msg;
        this.code = code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }
}
